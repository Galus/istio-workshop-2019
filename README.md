# istio-workshop-2019

@mt165's workshop from QConSF 

Full lesson [here](https://istio.mt165.training/)

``
gcloud services enable container.googleapis.com
gcloud container clusters create \
    --cluster-version 1.14 \
    --addons KubernetesDashboard \
    --machine-type n1-standard-4 \
    --num-nodes 1 \
    my-cluster

kubectl get componentstatuses
kubectl get pods

kubectl create clusterrolebinding my-admin \
    --user "$(gcloud config get-value core/account)" \
    --clusterrole cluster-admin

export ISTIO_VERSION=1.3.0  # Must set this otherwise we'll hit GitHub's API rate limit
curl -L https://git.io/getLatestIstio | sh




```
